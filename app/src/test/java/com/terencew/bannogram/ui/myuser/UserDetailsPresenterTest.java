package com.terencew.bannogram.ui.myuser;

import com.banno.terencew.instagram.model.InstagramUserDetails;
import com.banno.terencew.instagram.service.InstagramHelper;
import com.terencew.bannogram.di.ServiceLocator;
import com.terencew.bannogram.model.LocalRepository;

import org.greenrobot.eventbus.EventBus;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import io.reactivex.Single;

/**
 * Tests for UserDetailsPresenter
 */
public class UserDetailsPresenterTest {
    @Mock LocalRepository localRepo;
    @Mock InstagramHelper instagramHelper;
    @Mock UserDetailsPresenter.View view;
    @Mock EventBus bus;
    InstagramUserDetails userDetails = new InstagramUserDetails();
    UserDetailsPresenter presenter;

    @Before
    public void setup() {
        userDetails.setId("id"); // So .equals() works

        MockitoAnnotations.initMocks(this);

        Mockito.when(instagramHelper.getMyUserDetails()).thenReturn(Single.just(userDetails));

        ServiceLocator.setLocalRepository(localRepo);
        ServiceLocator.setInstagramHelper(instagramHelper);
        ServiceLocator.setBus(bus);

        presenter = new UserDetailsPresenter(view);
        presenter.attachView(view);
    }

    @Test
    public void loadDataAtStartTest() {
        presenter.start();

        Mockito.verify(view, Mockito.times(1)).displayUserDetails(userDetails);
    }

    @Test
    public void changeMediaTypeTest() {
        presenter.toggleImageSource();

        Mockito.verify(view, Mockito.times(1)).showMediaIcon(Mockito.any());
        Mockito.verify(bus, Mockito.times(1)).post(Mockito.any());
    }
}

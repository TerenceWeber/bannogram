package com.terencew.bannogram.ui.myuser;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.banno.terencew.instagram.model.InstagramUserDetails;
import com.squareup.picasso.Picasso;
import com.terencew.bannogram.R;
import com.terencew.bannogram.ui.transformations.CircularImageTransformation;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A view for showing a user's details
 */
public class UserDetailsFragment extends Fragment implements UserDetailsPresenter.View {
    public static UserDetailsFragment newInstance() {
        return new UserDetailsFragment();
    }

    @BindView(R.id.profile_pic) ImageView profilePic;
    @BindView(R.id.name) TextView name;
    @BindView(R.id.bio) TextView bio;
    @BindView(R.id.website) TextView website;
    @BindView(R.id.media_count) TextView mediaCount;
    @BindView(R.id.followed_by_count) TextView followedByCount;
    @BindView(R.id.follows_count) TextView followsCount;
    @BindView(R.id.image_toggle) ImageView imageToggle;
    private Unbinder unbinder;
    private UserDetailsPresenter presenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_user_details, container, false);

        unbinder = ButterKnife.bind(this, root);

        presenter.start();

        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        unbinder.unbind();
    }

    @OnClick(R.id.image_toggle) void imageToggleClicked() {
        presenter.toggleImageSource();
    }

    @Override
    public void setPresenter(UserDetailsPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void displayUserDetails(InstagramUserDetails userDetails) {
        name.setText(userDetails.getFullName());
        bio.setText(userDetails.getBio());
        website.setText(userDetails.getWebsite());
        mediaCount.setText(String.valueOf(userDetails.getMediaCounts().getMedia()));
        followsCount.setText(String.valueOf(userDetails.getMediaCounts().getFollows()));
        followedByCount.setText(String.valueOf(userDetails.getMediaCounts().getFollowedBy()));

        Picasso.with(getContext())
                .load(userDetails.getProfilePicUrl())
                .transform(new CircularImageTransformation())
                .into(profilePic);
    }

    @Override
    public void showMediaIcon(MediaSource mediaType) {
        int iconResId = mediaType == MediaSource.RECENT
                ? R.drawable.ic_portrait_white_36dp
                : R.drawable.ic_favorite_white_36dp;
        int tintResId = mediaType == MediaSource.RECENT
                ? R.color.toggle_mine
                : R.color.toggle_liked;

        Drawable drawable = ContextCompat.getDrawable(getContext(), iconResId);
        drawable = DrawableCompat.wrap(drawable);
        DrawableCompat.setTint(drawable.mutate(), ContextCompat.getColor(getContext(), tintResId));

        imageToggle.setImageDrawable(drawable);
    }
}

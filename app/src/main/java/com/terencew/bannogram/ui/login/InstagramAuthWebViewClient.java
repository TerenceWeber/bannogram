package com.terencew.bannogram.ui.login;

import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * A WebViewClient that examines redirects for Instagram auth tokens and notifies a listener
 */
public class InstagramAuthWebViewClient extends WebViewClient {
    private final AccessTokenParser parser;
    private final AuthListener listener;

    InstagramAuthWebViewClient(String redirectUri, AuthListener listener) {
        this.parser = new AccessTokenParser(redirectUri);
        this.listener = listener;
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        // If the URL contains our redirect URL then parse off the access_token
        // fragment and return it to the listener
        if (parser.hasAccessToken(url)) {
            String accessToken = parser.getAccessToken();
            listener.onUserAuthenticated(accessToken);
        } else {
            view.loadUrl(url);
        }

        return true;
    }
}

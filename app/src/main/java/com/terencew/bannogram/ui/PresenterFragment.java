package com.terencew.bannogram.ui;

import android.support.v4.app.Fragment;

/**
 * A way to retain a presenter across Activity configuration/orientation changes
 */
public class PresenterFragment<P extends BasePresenter> extends Fragment {
    private P presenter;

    public PresenterFragment() {
        setRetainInstance(true);
    }

    public P getPresenter() {
        return presenter;
    }

    public void setPresenter(P presenter) {
        this.presenter = presenter;
    }
}

package com.terencew.bannogram.ui.login;

import android.net.Uri;

/**
 * A parser for getting the access token out of an Instagram redirect URI
 */
class AccessTokenParser {
    private final String redirectUri;
    private String accessToken;

    AccessTokenParser(String redirectUri) {
        this.redirectUri = redirectUri;
    }

    boolean hasAccessToken(String url) {
        if (url.startsWith(redirectUri)) {
            Uri uri = Uri.parse(url);
            String fragment = uri.getFragment();

            // TODO: make this more intelligently parse out the #access_token=[token] fragment
            if (null != fragment && !fragment.isEmpty()) {
                this.accessToken = fragment.split("=")[1];
                return true;
            }
        }

        return false;
    }

    String getAccessToken() {
        return accessToken;
    }
}

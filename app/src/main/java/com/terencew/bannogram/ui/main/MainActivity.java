package com.terencew.bannogram.ui.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;

import com.terencew.bannogram.R;
import com.terencew.bannogram.ui.followed.FollowedFragment;
import com.terencew.bannogram.ui.myuser.MyUserFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A screen showing a set of tabs and the contents of the selected one
 */
public class MainActivity extends AppCompatActivity {
    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, MainActivity.class);

        return intent;
    }

    @BindView(R.id.bottom_nav) BottomNavigationView bottomNav;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Necessary for using vector drawables
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        bottomNav.setOnNavigationItemSelectedListener
                (item -> {
                    switch (item.getItemId()) {
                        case R.id.my_user:
                            showMyUser();
                            break;
                        case R.id.followed_users:
                            showMyFollowedUsers();
                            break;
                    }
                    return true;
                });

        // Start with the My User screen
        if (savedInstanceState == null) {
            showMyUser();
        }
    }

    private void showMyUser() {
        MyUserFragment fragment = MyUserFragment.newInstance();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .commit();
    }

    private void showMyFollowedUsers() {
        FollowedFragment fragment = FollowedFragment.newInstance();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .commit();
    }
}

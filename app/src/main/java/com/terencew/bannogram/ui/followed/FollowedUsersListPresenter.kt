package com.terencew.bannogram.ui.followed

import com.banno.terencew.instagram.model.InstagramUserDetails
import com.terencew.bannogram.di.ServiceLocator
import com.terencew.bannogram.ui.BasePresenter
import com.terencew.bannogram.ui.BaseView
import com.terencew.bannogram.ui.image.ImageSource
import com.terencew.bannogram.ui.image.ImageSourceSelectedEvent

/**
 * A presenter for dealing with a list of followed users
 */
class FollowedUsersListPresenter(view: FollowedUsersListPresenter.View) : BasePresenter<FollowedUsersListPresenter.View> {
    private var view: FollowedUsersListPresenter.View? = null
    private var followedUsers: List<InstagramUserDetails>? = null
    private var selectedUser: InstagramUserDetails? = null

    init {
        attachView(view)
    }

    override fun attachView(view: FollowedUsersListPresenter.View) {
        this.view = view
        view.setPresenter(this)
    }

    override fun detachView() {
        this.view = null
    }

    override fun start() {
        val followedUsers = this.followedUsers
        if (followedUsers == null) {
            loadFollowedUsers()
        } else {
            view?.displayUsers(followedUsers)
        }
    }

    /**
     * Retrieves the list of followed users from the network
     */
    private fun loadFollowedUsers() {
        ServiceLocator.getInstagramHelper().followedUsers
                .subscribe({ followedUsers -> onFollowedUsersLoaded(followedUsers) }
                ) { error ->
                    // TODO: handle errors
                }
    }

    private fun onFollowedUsersLoaded(followedUsers: List<InstagramUserDetails>) {
        // Save the list
        this.followedUsers = followedUsers

        // Display the list if we currently have a view
        view?.displayUsers(followedUsers)

        // Select the first user (if there is one)
        if (!followedUsers.isEmpty()) {
            selectUser(0)
        }
    }

    fun selectUser(index: Int) {
        val existingIndex = followedUsers?.indexOf(selectedUser)
        if (index == existingIndex)
            return

        val selectedUser = followedUsers!![index]
        this.selectedUser = selectedUser
        // TODO: view.highlightUser(index);

        // Post an event to the companion image collection screen
        val imageSource = createImageSourceForUser(selectedUser)
        ServiceLocator.getBus().post(ImageSourceSelectedEvent(imageSource))
    }

    /**
     * Creates an image source that gets the given user's recent media
     * @param selectedUser
     * *
     * @return
     */
    private fun createImageSourceForUser(selectedUser: InstagramUserDetails): ImageSource {
        return ImageSource { ServiceLocator.getInstagramHelper().getUserRecentMedia(selectedUser.id) }
    }

    interface View : BaseView<FollowedUsersListPresenter> {
        fun displayUsers(followedUsers: List<InstagramUserDetails>)
    }
}

package com.terencew.bannogram.ui.myuser;

import com.banno.terencew.instagram.model.InstagramUserDetails;
import com.banno.terencew.instagram.service.InstagramHelper;
import com.terencew.bannogram.di.ServiceLocator;
import com.terencew.bannogram.ui.BasePresenter;
import com.terencew.bannogram.ui.BaseView;
import com.terencew.bannogram.ui.image.ImageSource;
import com.terencew.bannogram.ui.image.ImageSourceSelectedEvent;

/**
 * A presenter for showing a user's details
 */
public class UserDetailsPresenter implements BasePresenter<UserDetailsPresenter.View> {
    private UserDetailsPresenter.View view;
    private InstagramUserDetails userDetails;
    private MediaSource displayedMediaType = MediaSource.RECENT;

    UserDetailsPresenter(UserDetailsPresenter.View view) {
        attachView(view);
    }

    @Override
    public void attachView(UserDetailsPresenter.View view) {
        this.view = view;
        this.view.setPresenter(this);
    }

    @Override
    public void detachView() {
        this.view = null;
    }

    @Override
    public void start() {
        if (userDetails == null) {
            loadUserDetails();
        } else {
            view.displayUserDetails(userDetails);
        }

        view.showMediaIcon(displayedMediaType);
    }

    private void loadUserDetails() {
        ServiceLocator.getInstagramHelper().getMyUserDetails()
                .subscribe(userDetails -> onUserDetailsLoaded(userDetails),
                            error -> {
                                // TODO: handle error
                            });
    }

    private void onUserDetailsLoaded(InstagramUserDetails userDetails) {
        this.userDetails = userDetails;
        if (view != null)
            view.displayUserDetails(userDetails);
    }

    public void toggleImageSource() {
        if (displayedMediaType == MediaSource.RECENT)
            displayedMediaType = MediaSource.LIKED;
        else
            displayedMediaType = MediaSource.RECENT;

        view.showMediaIcon(displayedMediaType);

        // Send an event to the companion image collection to use the new image source
        ImageSource imageSource = () -> {
            InstagramHelper instagramHelper = ServiceLocator.getInstagramHelper();

            if (displayedMediaType == MediaSource.RECENT)
                return instagramHelper.getMyRecentMedia();
            else
                return instagramHelper.getMyLikedMedia();
        };
        ImageSourceSelectedEvent event = new ImageSourceSelectedEvent(imageSource);
        ServiceLocator.getBus().post(event);
    }

    public interface View extends BaseView<UserDetailsPresenter> {
        void displayUserDetails(InstagramUserDetails userDetails);
        void showMediaIcon(MediaSource mediaType);
    }
}

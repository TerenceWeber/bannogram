package com.terencew.bannogram.ui.image;

/**
 * An event sent to an ImageCollectionPresenter when the source of its images should change
 */
public class ImageSourceSelectedEvent {
    private final ImageSource source;

    public ImageSourceSelectedEvent(ImageSource source) {
        this.source = source;
    }

    public ImageSource getSource() {
        return source;
    }
}

package com.terencew.bannogram.ui.image;

import com.banno.terencew.instagram.model.InstagramMedia;

import java.util.List;

import io.reactivex.Single;

/**
 * An interface for getting a list of Instagram images
 */
public interface ImageSource {
    Single<List<InstagramMedia>> getImages();
}

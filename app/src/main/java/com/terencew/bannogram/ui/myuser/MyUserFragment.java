package com.terencew.bannogram.ui.myuser;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.terencew.bannogram.ui.PresenterFragment;
import com.terencew.bannogram.R;
import com.terencew.bannogram.di.ServiceLocator;
import com.terencew.bannogram.ui.image.ImageCollectionFragment;
import com.terencew.bannogram.ui.image.ImageCollectionPresenter;

/**
 * Container fragment for showing the user's profile and their images
 */
public class MyUserFragment extends Fragment {
    public static MyUserFragment newInstance() {
        return new MyUserFragment();
    }

    private static final String FRAGMENT_TAG_USER_DETAILS_VIEW = "FRAGMENT_TAG_USER_DETAILS_VIEW";
    private static final String FRAGMENT_TAG_USER_DETAILS_PRESENTER = "FRAGMENT_TAG_USER_DETAILS_PRESENTER";
    private static final String FRAGMENT_TAG_IMAGES_VIEW = "FRAGMENT_TAG_IMAGES_VIEW";
    private static final String FRAGMENT_TAG_IMAGES_PRESENTER = "FRAGMENT_TAG_IMAGES_PRESENTER";

    private UserDetailsFragment userView;
    private UserDetailsPresenter userPresenter;
    private ImageCollectionFragment imagesView;
    private ImageCollectionPresenter imagesPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            userView = UserDetailsFragment.newInstance();
            userPresenter = new UserDetailsPresenter(userView);
            PresenterFragment<UserDetailsPresenter> userPresenterFragment = new PresenterFragment<>();
            userPresenterFragment.setPresenter(userPresenter);

            imagesView = ImageCollectionFragment.newInstance();
            imagesPresenter = new ImageCollectionPresenter();
            imagesPresenter.setImageSource(() -> ServiceLocator.getInstagramHelper().getMyRecentMedia());
            PresenterFragment<ImageCollectionPresenter> imagesPresenterFragment = new PresenterFragment<>();
            imagesPresenterFragment.setPresenter(imagesPresenter);

            getChildFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment_container_user_details, userView, FRAGMENT_TAG_USER_DETAILS_VIEW)
                    .add(userPresenterFragment, FRAGMENT_TAG_USER_DETAILS_PRESENTER)
                    .add(R.id.fragment_container_images, imagesView, FRAGMENT_TAG_IMAGES_VIEW)
                    .add(imagesPresenterFragment, FRAGMENT_TAG_IMAGES_PRESENTER)
                    .commit();
        } else {
            userView = (UserDetailsFragment) getChildFragmentManager().findFragmentByTag(FRAGMENT_TAG_USER_DETAILS_VIEW);
            PresenterFragment<UserDetailsPresenter> userPresenterFragment = (PresenterFragment<UserDetailsPresenter>) getChildFragmentManager()
                    .findFragmentByTag(FRAGMENT_TAG_USER_DETAILS_PRESENTER);
            userPresenter = userPresenterFragment.getPresenter();

            imagesView = (ImageCollectionFragment) getChildFragmentManager().findFragmentByTag(FRAGMENT_TAG_IMAGES_VIEW);
            PresenterFragment<ImageCollectionPresenter> imagesPresenterFragment = (PresenterFragment<ImageCollectionPresenter>) getChildFragmentManager()
                    .findFragmentByTag(FRAGMENT_TAG_IMAGES_PRESENTER);
            imagesPresenter = imagesPresenterFragment.getPresenter();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_my_user, container, false);

        userPresenter.attachView(userView);
        imagesPresenter.attachView(imagesView);

        return root;
    }
}

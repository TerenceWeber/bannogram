package com.terencew.bannogram.ui;

/**
 * A base presenter class in MVP architecture
 */
public interface BasePresenter<V extends BaseView> {
    void attachView(V view);
    void detachView();
    void start();
}

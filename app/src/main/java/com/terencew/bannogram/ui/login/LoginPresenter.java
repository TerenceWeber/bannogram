package com.terencew.bannogram.ui.login;

import com.terencew.bannogram.di.ServiceLocator;
import com.terencew.bannogram.model.User;
import com.terencew.bannogram.ui.BasePresenter;
import com.terencew.bannogram.ui.BaseView;

/**
 * An MVP presenter for the login screen
 */
public class LoginPresenter implements BasePresenter<LoginPresenter.View> {
    private LoginPresenter.View view;
    private boolean hasShownWebView = false;

    LoginPresenter(LoginPresenter.View view) {
        attachView(view);
    }

    @Override
    public void attachView(LoginPresenter.View view) {
        this.view = view;
        this.view.setPresenter(this);
    }

    @Override
    public void detachView() {
        view = null;
    }

    @Override
    public void start() {
        if (hasShownWebView) {
            view.showWebView();
        }
    }

    public void onClickedLoginButton() {
        hasShownWebView = true;
        view.showWebView();
    }

    public void saveUser(String accessToken) {
        User user = new User(accessToken);
        ServiceLocator.getLocalRepository().saveUser(user);
        view.goToMainScreen();
    }

    public interface View extends BaseView<LoginPresenter> {
        void showWebView();
        void goToMainScreen();
    }
}

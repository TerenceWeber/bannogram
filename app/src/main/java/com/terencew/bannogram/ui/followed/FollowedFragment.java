package com.terencew.bannogram.ui.followed;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.terencew.bannogram.R;
import com.terencew.bannogram.ui.PresenterFragment;
import com.terencew.bannogram.ui.image.ImageCollectionFragment;
import com.terencew.bannogram.ui.image.ImageCollectionPresenter;

/**
 * A view for showing all of the users that the current user is following and their media
 */
public class FollowedFragment extends Fragment {
    public static FollowedFragment newInstance() {
        return new FollowedFragment();
    }

    private static final String FRAGMENT_TAG_USER_LIST_VIEW = "FRAGMENT_TAG_USER_LIST_VIEW";
    private static final String FRAGMENT_TAG_USER_LIST_PRESENTER = "FRAGMENT_TAG_USER_LIST_PRESENTER";
    private static final String FRAGMENT_TAG_IMAGES_VIEW = "FRAGMENT_TAG_IMAGES_VIEW";
    private static final String FRAGMENT_TAG_IMAGES_PRESENTER = "FRAGMENT_TAG_IMAGES_PRESENTER";

    private FollowedUsersListFragment followedUsersListView;
    private FollowedUsersListPresenter followedUsersListPresenter;
    private ImageCollectionFragment imagesView;
    private ImageCollectionPresenter imagesPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            // Set up the list of followed users
            followedUsersListView = FollowedUsersListFragment.newInstance();
            followedUsersListPresenter = new FollowedUsersListPresenter(followedUsersListView);
            PresenterFragment<FollowedUsersListPresenter> userPresenterFragment = new PresenterFragment<>();
            userPresenterFragment.setPresenter(followedUsersListPresenter);

            // Set up the images for the selected user
            imagesView = ImageCollectionFragment.newInstance();
            imagesPresenter = new ImageCollectionPresenter();
            PresenterFragment<ImageCollectionPresenter> imagesPresenterFragment = new PresenterFragment<>();
            imagesPresenterFragment.setPresenter(imagesPresenter);

            getChildFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment_container_user_list, followedUsersListView, FRAGMENT_TAG_USER_LIST_VIEW)
                    .add(userPresenterFragment, FRAGMENT_TAG_USER_LIST_PRESENTER)
                    .add(R.id.fragment_container_images, imagesView, FRAGMENT_TAG_IMAGES_VIEW)
                    .add(imagesPresenterFragment, FRAGMENT_TAG_IMAGES_PRESENTER)
                    .commit();
        } else {
            // Set up the list of followed users
            followedUsersListView = (FollowedUsersListFragment) getChildFragmentManager().findFragmentByTag(FRAGMENT_TAG_USER_LIST_VIEW);
            PresenterFragment<FollowedUsersListPresenter> userPresenterFragment = (PresenterFragment<FollowedUsersListPresenter>) getChildFragmentManager()
                    .findFragmentByTag(FRAGMENT_TAG_USER_LIST_PRESENTER);
            followedUsersListPresenter = userPresenterFragment.getPresenter();

            // Set up the images for the selected user
            imagesView = (ImageCollectionFragment) getChildFragmentManager().findFragmentByTag(FRAGMENT_TAG_IMAGES_VIEW);
            PresenterFragment<ImageCollectionPresenter> imagesPresenterFragment = (PresenterFragment<ImageCollectionPresenter>) getChildFragmentManager()
                    .findFragmentByTag(FRAGMENT_TAG_IMAGES_PRESENTER);
            imagesPresenter = imagesPresenterFragment.getPresenter();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_followed_users, container, false);

        followedUsersListPresenter.attachView(followedUsersListView);
        imagesPresenter.attachView(imagesView);

        return root;
    }
}

package com.terencew.bannogram.ui.myuser;

/**
 * Options for a user's image source
 */
public enum MediaSource {
    RECENT,
    LIKED
}

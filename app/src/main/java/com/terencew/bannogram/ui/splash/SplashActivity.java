package com.terencew.bannogram.ui.splash;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.terencew.bannogram.BannogramApplication;
import com.terencew.bannogram.di.ServiceLocator;
import com.terencew.bannogram.ui.login.LoginActivity;
import com.terencew.bannogram.ui.main.MainActivity;

/**
 * Initial Activity
 * Checks if the user is logged in and loads the next screen accordingly
 */
public class SplashActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (isUserLoggedIn()) {
            goToMainScreen();
        } else {
            goToLoginScreen();
        }
    }

    private boolean isUserLoggedIn() {
        return ServiceLocator.getLocalRepository().isUserLoggedIn();
    }

    private void goToMainScreen() {
        Intent intent = MainActivity.newIntent(BannogramApplication.current());
        startActivity(intent);
    }

    private void goToLoginScreen() {
        Intent intent = LoginActivity.newIntent(BannogramApplication.current());
        startActivity(intent);
    }
}

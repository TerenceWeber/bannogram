package com.terencew.bannogram.ui.image;

import com.banno.terencew.instagram.model.InstagramMedia;
import com.terencew.bannogram.di.ServiceLocator;
import com.terencew.bannogram.ui.BasePresenter;
import com.terencew.bannogram.ui.BaseView;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

/**
 * A presenter for showing a collection of Instagram images
 */
public class ImageCollectionPresenter implements BasePresenter<ImageCollectionPresenter.View> {
    private ImageCollectionPresenter.View view;
    private List<InstagramMedia> images;
    private ImageSource imageSource;

    @Override
    public void attachView(ImageCollectionPresenter.View view) {
        this.view = view;
        this.view.setPresenter(this);

        ServiceLocator.getBus().register(this);
    }

    @Override
    public void detachView() {
        ServiceLocator.getBus().unregister(this);

        this.view = null;
    }

    @Override
    public void start() {
        if (images == null) {
            view.showEmptyView(false);
            view.showLoadingIndicator(true);
            loadNextPage();
        } else if (images.isEmpty()) {
            view.showEmptyView(true);
            view.showLoadingIndicator(false);
        } else {
            view.showEmptyView(false);
            view.showLoadingIndicator(false);
            view.showImages(images);
        }
    }

    @Subscribe
    public void onImageSourceChanged(ImageSourceSelectedEvent event) {
        updateImageSource(event.getSource());
    }

    private void updateImageSource(ImageSource imageSource) {
        setImageSource(imageSource);
        loadNextPage();
    }

    public void setImageSource(ImageSource imageSource) {
        this.imageSource = imageSource;
        images = null;

        if (view != null) {
            view.clearList();
            view.showLoadingIndicator(true);
        }
    }

    private void loadNextPage() {
        if (imageSource == null)
            return;

        imageSource.getImages()
            .subscribe(result -> addPage(result),
                    error -> {});
    }

    private void addPage(List<InstagramMedia> nextPage) {
        if (images == null)
            images = new ArrayList<>();
        images.addAll(nextPage);

        if (view != null) {
            view.showImages(nextPage);
            view.showEmptyView(false);
            view.showLoadingIndicator(false);
        }
    }

    public void likeImage(int index) {
        InstagramMedia image = images.get(index);
        boolean newLikeState = !image.hasUserLiked();
        image.setHasUserLiked(newLikeState);

        view.setImageLikeState(index, newLikeState);

        ServiceLocator.getInstagramHelper().likeMedia(image.getId(), newLikeState)
                .subscribe(result -> {
                            // Do nothing on result.. we already updated the model and the view
                        },
                        error -> {
                            // TODO: post an event to reset the state
                        });
    }

    public interface View extends BaseView<ImageCollectionPresenter> {
        void showLoadingIndicator(boolean show);
        void clearList();
        void showEmptyView(boolean show);
        void showImages(List<InstagramMedia> images);
        void setImageLikeState(int index, boolean likeState);
    }
}

package com.terencew.bannogram.ui.image;

import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.banno.terencew.instagram.model.InstagramImage;
import com.banno.terencew.instagram.model.InstagramMedia;
import com.squareup.picasso.Picasso;
import com.terencew.bannogram.R;
import com.terencew.bannogram.ui.transformations.CircularImageTransformation;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * An adapter showing a list of Instagram images
 */
public class ImageCollectionAdapter extends RecyclerView.Adapter<ImageCollectionAdapter.ImageViewHolder> {
    /**
     * A listener for when an image is interacted with by the user
     */
    public interface Listener {
        void onLikeIconTapped(int index);
    }

    private List<InstagramMedia> images = new ArrayList<>();
    private Listener listener;

    ImageCollectionAdapter(Listener listener) {
        this.listener = listener;
    }

    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View root = inflater.inflate(R.layout.item_image, parent, false);

        return new ImageViewHolder(root);
    }

    @Override
    public void onBindViewHolder(ImageViewHolder holder, int position) {
        InstagramMedia image = images.get(position);
        holder.bind(image);
    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    public void add(List<InstagramMedia> images) {
        this.images.addAll(images);
        this.notifyItemRangeInserted(this.images.size() - images.size(), images.size());
    }

    void clear() {
        this.images.clear();
        this.notifyDataSetChanged();
    }

    class ImageViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.avatar) ImageView avatar;
        @BindView(R.id.picture) AspectRatioImageView picture;
        @BindView(R.id.caption) TextView caption;
        @BindView(R.id.location) TextView location;
        @BindView(R.id.user) TextView user;
        @BindView(R.id.like) ImageView like;

        ImageViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }

        void bind(InstagramMedia image) {
            Picasso.with(itemView.getContext())
                    .load(image.getUser().getProfilePicUrl())
                    .transform(new CircularImageTransformation())
                    .into(avatar);

            user.setText(image.getUser().getUsername());

            if (image.getLocation() == null || TextUtils.isEmpty(image.getLocation().getName())) {
                location.setVisibility(View.GONE);
            } else {
                location.setVisibility(View.VISIBLE);
                location.setText(image.getLocation().getName());
            }

            if (image.getCaption() == null || TextUtils.isEmpty(image.getCaption().getText())) {
                caption.setVisibility(View.GONE);
            } else {
                caption.setVisibility(View.VISIBLE);
                caption.setText(image.getCaption().getText());
            }

            updateLikeIcon(image.hasUserLiked());

            // Preset the aspect ratio so the ImageView allocates enough room before the image loads
            InstagramImage standardImage = image.getImages().getStandardResolution();
            picture.setAspectRatio((float) standardImage.getWidth() / standardImage.getHeight());
            Picasso.with(itemView.getContext())
                    .load(standardImage.getUrl())
                    .into(picture);
        }

        @OnClick(R.id.like) void tappedLikeIcon() {
            if (listener != null)
                listener.onLikeIconTapped(getAdapterPosition());
        }

        void updateLikeIcon(boolean liked) {
            int iconResId = liked
                    ? R.drawable.ic_favorite_white_36dp
                    : R.drawable.ic_favorite_border_white_36dp;
            int tintResId = liked
                    ? R.color.liked_tint
                    : R.color.unliked_tint;

            Drawable drawable = ContextCompat.getDrawable(itemView.getContext(), iconResId);
            drawable = DrawableCompat.wrap(drawable);
            DrawableCompat.setTint(drawable.mutate(), ContextCompat.getColor(itemView.getContext(), tintResId));

            like.setImageDrawable(drawable);
        }
    }
}

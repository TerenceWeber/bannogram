package com.terencew.bannogram.ui.login;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;

import com.terencew.bannogram.BannogramApplication;
import com.terencew.bannogram.R;
import com.terencew.bannogram.ui.main.MainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * An MVP view for the login screen
 */
public class LoginFragment extends Fragment implements LoginPresenter.View, AuthListener {
    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @BindView(R.id.login) Button login;
    @BindView(R.id.webview) WebView webView;
    private Unbinder unbinder;
    private LoginPresenter presenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_login, container, false);

        unbinder = ButterKnife.bind(this, root);

        String redirectUri = getString(R.string.instagram_redirect_uri);
        webView.setWebViewClient(new InstagramAuthWebViewClient(redirectUri,this));
        if (savedInstanceState == null) {
            String clientId = getString(R.string.instagram_client_id);
            String scope = getString(R.string.instagram_permissions_scope);
            String url = getString(R.string.instagram_auth_url, clientId, redirectUri, scope);
            webView.loadUrl(url);
        }

        presenter.start();

        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        presenter.detachView();
        unbinder.unbind();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        webView.saveState(outState);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);

        webView.restoreState(savedInstanceState);
    }

    @Override
    public void setPresenter(LoginPresenter presenter) {
        this.presenter = presenter;
    }

    @OnClick(R.id.login) void loginClicked() {
        this.presenter.onClickedLoginButton();
    }

    @Override
    public void showWebView() {
        webView.setVisibility(View.VISIBLE);

        // Allow the WebView to handle going back
        webView.setOnKeyListener((v, keyCode, event) -> {
            if (event.getAction() == KeyEvent.ACTION_DOWN) {
                WebView webView = (WebView) v;
                switch(keyCode) {
                    case KeyEvent.KEYCODE_BACK:
                        if (webView.canGoBack()) {
                            webView.goBack();
                            return true;
                        }
                        break;
                }
            }

            return false;
        });
    }

    @Override
    public void onUserAuthenticated(String accessToken) {
        presenter.saveUser(accessToken);
    }

    @Override
    public void goToMainScreen() {
        startActivity(MainActivity.newIntent(BannogramApplication.current()));
    }
}

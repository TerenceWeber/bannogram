package com.terencew.bannogram.ui.image;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.banno.terencew.instagram.model.InstagramMedia;
import com.terencew.bannogram.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A view showing a collection of Instagram images
 */
public class ImageCollectionFragment extends Fragment implements ImageCollectionPresenter.View, ImageCollectionAdapter.Listener {
    public static ImageCollectionFragment newInstance() {
        return new ImageCollectionFragment();
    }

    @BindView(R.id.loading_indicator) View loadingIndicator;
    @BindView(R.id.images) RecyclerView images;
    private Unbinder unbinder;
    private ImageCollectionAdapter adapter;
    private ImageCollectionPresenter presenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_images, container, false);

        unbinder = ButterKnife.bind(this, root);

        adapter = new ImageCollectionAdapter(this);

        images.setItemAnimator(new DefaultItemAnimator());
        int spanCount = getResources().getInteger(R.integer.image_collection_columns);
        images.setLayoutManager(new GridLayoutManager(getContext(), spanCount));
        images.setAdapter(adapter);

        presenter.start();

        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        presenter.detachView();

        unbinder.unbind();
    }

    @Override
    public void setPresenter(ImageCollectionPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void showLoadingIndicator(boolean show) {
        loadingIndicator.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void clearList() {
        adapter.clear();
    }

    @Override
    public void showEmptyView(boolean show) {
        // TODO: implement
    }

    @Override
    public void showImages(List<InstagramMedia> images) {
        adapter.add(images);
    }

    @Override
    public void onLikeIconTapped(int index) {
        presenter.likeImage(index);
    }

    @Override
    public void setImageLikeState(int index, boolean likeState) {
        ImageCollectionAdapter.ImageViewHolder viewHolder =
                (ImageCollectionAdapter.ImageViewHolder) images.findViewHolderForAdapterPosition(index);
        viewHolder.updateLikeIcon(likeState);
    }
}

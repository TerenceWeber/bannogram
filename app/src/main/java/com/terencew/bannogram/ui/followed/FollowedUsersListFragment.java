package com.terencew.bannogram.ui.followed;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.banno.terencew.instagram.model.InstagramUserDetails;
import com.terencew.bannogram.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A screen showing a selectable list of followed users
 */
public class FollowedUsersListFragment extends Fragment implements FollowedUsersListPresenter.View, FollowedUsersAdapter.Listener {
    public static FollowedUsersListFragment newInstance() {
        return new FollowedUsersListFragment();
    }

    @BindView(R.id.followed_users) RecyclerView followedUsers;
    private Unbinder unbinder;
    private FollowedUsersAdapter adapter = new FollowedUsersAdapter(this);
    private FollowedUsersListPresenter presenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_followed_users_list, container, false);

        unbinder = ButterKnife.bind(this, root);

        followedUsers.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        followedUsers.setItemAnimator(new DefaultItemAnimator());
        followedUsers.setAdapter(adapter);

        presenter.start();

        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        unbinder.unbind();
    }

    @Override
    public void setPresenter(FollowedUsersListPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void displayUsers(List<? extends InstagramUserDetails> followedUsers) {
        adapter.add(followedUsers);
    }

    @Override
    public void onFollowedUserSelected(int index) {
        presenter.selectUser(index);
    }
}

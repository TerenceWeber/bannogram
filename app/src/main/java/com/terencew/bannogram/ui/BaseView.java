package com.terencew.bannogram.ui;

/**
 * A base view class in MVP architecture
 */
public interface BaseView<P extends BasePresenter> {
    void setPresenter(P presenter);
}

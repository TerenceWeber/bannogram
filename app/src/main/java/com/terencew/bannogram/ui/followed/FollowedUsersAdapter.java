package com.terencew.bannogram.ui.followed;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.banno.terencew.instagram.model.InstagramUserDetails;
import com.squareup.picasso.Picasso;
import com.terencew.bannogram.R;
import com.terencew.bannogram.ui.transformations.CircularImageTransformation;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * An adapter showing a list of followed users
 */
public class FollowedUsersAdapter extends RecyclerView.Adapter<FollowedUsersAdapter.FollowedUserViewHolder> {
    // A listener for when action is taken on a list entry
    public interface Listener {
        void onFollowedUserSelected(int index);
    }

    private Listener listener;
    private List<InstagramUserDetails> followedUsers = new ArrayList<>();

    FollowedUsersAdapter(Listener listener) {
        this.listener = listener;
    }

    @Override
    public FollowedUserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View root = inflater.inflate(R.layout.item_followed_user, parent, false);

        return new FollowedUserViewHolder(root);
    }

    @Override
    public void onBindViewHolder(FollowedUserViewHolder holder, int position) {
        InstagramUserDetails followedUser = followedUsers.get(position);
        holder.bind(followedUser);
    }

    @Override
    public int getItemCount() {
        return followedUsers.size();
    }

    public void add(List<? extends InstagramUserDetails> newUsers) {
        this.followedUsers.addAll(newUsers);
        this.notifyItemRangeInserted(this.followedUsers.size() - newUsers.size(), newUsers.size());
    }

    class FollowedUserViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.avatar) ImageView avatar;

        FollowedUserViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }

        void bind(InstagramUserDetails followedUser) {
            Picasso.with(itemView.getContext())
                    .load(followedUser.getProfilePicUrl())
                    .transform(new CircularImageTransformation())
                    .into(avatar);

            itemView.setOnClickListener(v -> {
                if (listener != null) {
                    listener.onFollowedUserSelected(getAdapterPosition());
                }
            });
        }
    }
}

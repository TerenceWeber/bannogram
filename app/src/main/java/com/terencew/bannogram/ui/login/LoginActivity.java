package com.terencew.bannogram.ui.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.terencew.bannogram.ui.PresenterFragment;
import com.terencew.bannogram.R;

/**
 * A screen allowing the user to authenticate with Instagram
 */
public class LoginActivity extends AppCompatActivity {
    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, LoginActivity.class);

        return intent;
    }

    private static final String FRAGMENT_TAG_VIEW = "FRAGMENT_TAG_VIEW";
    private static final String FRAGMENT_TAG_PRESENTER = "FRAGMENT_TAG_PRESENTER";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        if (savedInstanceState == null) {
            LoginFragment view = LoginFragment.newInstance();

            LoginPresenter presenter = new LoginPresenter(view);
            PresenterFragment<LoginPresenter> presenterFragment = new PresenterFragment<>();
            presenterFragment.setPresenter(presenter);

            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment_container, view, FRAGMENT_TAG_VIEW)
                    .add(presenterFragment, FRAGMENT_TAG_PRESENTER)
                    .commit();
        } else {
            LoginFragment view = (LoginFragment) getSupportFragmentManager().findFragmentByTag(FRAGMENT_TAG_VIEW);
            PresenterFragment<LoginPresenter> presenterFragment =
                    (PresenterFragment<LoginPresenter>) getSupportFragmentManager().findFragmentByTag(FRAGMENT_TAG_PRESENTER);
            presenterFragment.getPresenter().attachView(view);
        }
    }
}

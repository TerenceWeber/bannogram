package com.terencew.bannogram.ui.login;

/**
 * A callback for when the user has successfully logged into Instagram
 */
public interface AuthListener {
    void onUserAuthenticated(String accessToken);
}

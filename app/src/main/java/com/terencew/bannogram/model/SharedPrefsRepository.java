package com.terencew.bannogram.model;

import android.content.Context;
import android.content.SharedPreferences;

import com.terencew.bannogram.BannogramApplication;

/**
 * A cheap way to save objects locally.
 */
public class SharedPrefsRepository implements LocalRepository {
    private static final String SHARED_PREFS_NAME = "SharedPrefsRepository";
    private static final String PREF_KEY_AUTH_TOKEN = "PREF_KEY_AUTH_TOKEN";

    @Override
    public boolean isUserLoggedIn() {
        return getUser() != null;
    }

    @Override
    public User getUser() {
        SharedPreferences prefs = getPrefs();
        String authToken = prefs.getString(PREF_KEY_AUTH_TOKEN, null);

        return authToken == null
                ? null
                : new User(authToken);
    }

    @Override
    public void saveUser(User user) {
        SharedPreferences.Editor editor = getPrefs().edit();
        editor.putString(PREF_KEY_AUTH_TOKEN, user.getAccessToken());
        editor.apply();
    }

    private SharedPreferences getPrefs() {
        return BannogramApplication
                .current()
                .getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE);
    }
}

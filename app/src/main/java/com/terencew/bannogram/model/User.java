package com.terencew.bannogram.model;

/**
 * The currently logged in user
 * Currently just stores the Instagram access_token
 */
public class User {
    private String accessToken;

    public User(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getAccessToken() {
        return accessToken;
    }
}

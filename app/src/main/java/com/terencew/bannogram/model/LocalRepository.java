package com.terencew.bannogram.model;

/**
 * An interface for saving and retrieving objects locally
 */
public interface LocalRepository {
    boolean isUserLoggedIn();
    User getUser();
    void saveUser(User user);
}

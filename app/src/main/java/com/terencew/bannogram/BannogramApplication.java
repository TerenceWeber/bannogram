package com.terencew.bannogram;

import android.app.Application;

/**
 * Application class
 */
public class BannogramApplication extends Application {
    private static BannogramApplication instance;
    public static BannogramApplication current() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;
    }
}

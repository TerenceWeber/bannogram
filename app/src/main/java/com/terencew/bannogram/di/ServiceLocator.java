package com.terencew.bannogram.di;

import com.banno.terencew.instagram.service.InstagramHelper;
import com.terencew.bannogram.model.LocalRepository;
import com.terencew.bannogram.model.SharedPrefsRepository;
import com.terencew.bannogram.model.User;

import org.greenrobot.eventbus.EventBus;

/**
 * A service locator. In a real app this would be a better form of dependency injection (Dagger 2)
 */
public class ServiceLocator {
    private static LocalRepository localRepository;
    private static InstagramHelper instagramHelper;
    private static EventBus bus;

    public static InstagramHelper getInstagramHelper() {
        if (instagramHelper == null) {
            User user = getLocalRepository().getUser();
            String accessToken = user.getAccessToken();
            instagramHelper = new InstagramHelper(accessToken);
        }

        return instagramHelper;
    }

    public static void setInstagramHelper(InstagramHelper instagramHelper) {
        ServiceLocator.instagramHelper = instagramHelper;
    }

    public static LocalRepository getLocalRepository() {
        if (localRepository == null) {
            localRepository = new SharedPrefsRepository();
        }

        return localRepository;
    }

    public static void setLocalRepository(LocalRepository localRepository) {
        ServiceLocator.localRepository = localRepository;
    }

    public static EventBus getBus() {
        if (bus == null) {
            bus = EventBus.getDefault();
        }

        return bus;
    }

    public static void setBus(EventBus bus) {
        ServiceLocator.bus = bus;
    }
}

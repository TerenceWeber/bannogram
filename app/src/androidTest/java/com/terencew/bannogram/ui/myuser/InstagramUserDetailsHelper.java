package com.terencew.bannogram.ui.myuser;

import com.banno.terencew.instagram.model.InstagramMediaCounts;
import com.banno.terencew.instagram.model.InstagramUserDetails;

/**
 * A helper for getting an instance of InstagramUserDetail
 */
public class InstagramUserDetailsHelper {
    public static InstagramUserDetails getExample() {
        InstagramUserDetails userDetails = new InstagramUserDetails();
        userDetails = new InstagramUserDetails();
        userDetails.setId("id");
        userDetails.setBio("bio");
        userDetails.setFullName("full name");
        userDetails.setUsername("user name");
        userDetails.setWebsite("http://www.google.com");
        userDetails.setProfilePicUrl("https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png");
        InstagramMediaCounts mediaCounts = new InstagramMediaCounts();
        mediaCounts.setFollowedBy(5);
        mediaCounts.setFollows(6);
        mediaCounts.setMedia(7);
        userDetails.setMediaCounts(mediaCounts);

        return userDetails;
    }
}

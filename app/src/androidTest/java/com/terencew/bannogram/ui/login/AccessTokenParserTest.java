package com.terencew.bannogram.ui.login;

import android.support.test.runner.AndroidJUnit4;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.UUID;

/**
 * Tests for AccessTokenParser
 */
@RunWith(AndroidJUnit4.class)
public class AccessTokenParserTest {
    @Test
    public void testWrongUrl() {
        String redirectUri = "www.test.com";
        String loadedUrl = "www.somethingelse.com";

        AccessTokenParser parser = new AccessTokenParser(redirectUri);
        Assert.assertFalse(parser.hasAccessToken(loadedUrl));
    }

    @Test
    public void testWithToken() {
        String token = UUID.randomUUID().toString();
        String redirectUri = "www.test.com";
        String loadedUrl = redirectUri + "#access_token=" + token;

        AccessTokenParser parser = new AccessTokenParser(redirectUri);
        Assert.assertTrue(parser.hasAccessToken(loadedUrl));
        Assert.assertTrue(parser.getAccessToken().equals(token));
    }
}

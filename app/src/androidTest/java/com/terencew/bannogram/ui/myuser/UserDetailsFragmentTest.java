package com.terencew.bannogram.ui.myuser;

import android.graphics.drawable.Drawable;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.widget.ImageView;

import com.banno.terencew.instagram.model.InstagramUserDetails;
import com.banno.terencew.instagram.service.InstagramHelper;
import com.terencew.bannogram.R;
import com.terencew.bannogram.di.ServiceLocator;
import com.terencew.bannogram.model.LocalRepository;
import com.terencew.bannogram.model.User;
import com.terencew.bannogram.ui.main.MainActivity;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;

import io.reactivex.Single;

import static android.support.test.InstrumentationRegistry.getTargetContext;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Tests for UserDetailsFragment
 */
@RunWith(AndroidJUnit4.class)
public class UserDetailsFragmentTest {
    @Rule
    public final ActivityTestRule<MainActivity> main = new ActivityTestRule<>(MainActivity.class, false, false);

    InstagramUserDetails userDetails = InstagramUserDetailsHelper.getExample();
    @Mock LocalRepository localRepo;
    @Mock InstagramHelper instagramHelper;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        Mockito.when(localRepo.isUserLoggedIn()).thenReturn(true);
        Mockito.when(localRepo.getUser()).thenReturn(new User(""));
        Mockito.when(instagramHelper.getMyUserDetails()).thenReturn(Single.just(userDetails));
        Mockito.when(instagramHelper.getMyRecentMedia()).thenReturn(Single.just(new ArrayList<>()));

        ServiceLocator.setLocalRepository(localRepo);
        ServiceLocator.setInstagramHelper(instagramHelper);

        main.launchActivity(MainActivity.newIntent(getTargetContext()));
    }

    @Test
    public void profileValuesSetCorrectlyTest() {
        // The mock provider immediately returns user details, so just check it right away
        onView(withId(R.id.bio)).check(matches(withText(userDetails.getBio())));
        onView(withId(R.id.name)).check(matches(withText(userDetails.getFullName())));
        onView(withId(R.id.website)).check(matches(withText(userDetails.getWebsite())));
        onView(withId(R.id.followed_by_count)).check(matches(withText(String.valueOf(userDetails.getMediaCounts().getFollowedBy()))));
        onView(withId(R.id.follows_count)).check(matches(withText(String.valueOf(userDetails.getMediaCounts().getFollows()))));
        onView(withId(R.id.media_count)).check(matches(withText(String.valueOf(userDetails.getMediaCounts().getMedia()))));
    }

    @Test
    public void imageToggleTest() {
        // Verify that the toggle icon changes vector drawables
        ImageView imageToggle = (ImageView) main.getActivity().findViewById(R.id.image_toggle);
        Drawable.ConstantState initialState = imageToggle.getDrawable().getConstantState();
        onView(withId(R.id.image_toggle)).perform(click());
        Drawable.ConstantState newState = imageToggle.getDrawable().getConstantState();
        Assert.assertNotSame(initialState, newState);
    }
}

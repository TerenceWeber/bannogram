package com.terencew.bannogram.model;

import android.support.test.runner.AndroidJUnit4;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.UUID;

/**
 * Tests for SharedPrefsRepository
 */
@RunWith(AndroidJUnit4.class)
public class SharedPrefsRepositoryTest {
    @Test
    public void testSavingAndRetrievingUser() {
        String accessToken = UUID.randomUUID().toString();
        User user = new User(accessToken);

        SharedPrefsRepository repo = new SharedPrefsRepository();
        repo.saveUser(user);

        User retrievedUser = repo.getUser();

        Assert.assertEquals(user.getAccessToken(), retrievedUser.getAccessToken());
    }
}

package com.banno.terencew.instagram.service;

import com.banno.terencew.instagram.model.InstagramMedia;
import com.banno.terencew.instagram.model.InstagramResponse;
import com.banno.terencew.instagram.model.InstagramUserDetails;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * A Retrofit interface for interacting with Instagram's REST API
 * TODO: maybe add a request interceptor to add the access_token to every call
 */
interface InstagramService {
    @GET("users/self/")
    Single<InstagramResponse<InstagramUserDetails>> getMyUserDetails(
            @Query("access_token") String accessToken);

    @GET("users/{userId}/")
    Single<InstagramResponse<InstagramUserDetails>> getUserDetails(
            @Path("userId") String userId,
            @Query("access_token") String accessToken);

    @GET("users/self/media/recent")
    Single<InstagramResponse<List<InstagramMedia>>> getMyRecentMedia(
            @Query("access_token") String accessToken);

    @GET("users/{userId}/media/recent")
    Single<InstagramResponse<List<InstagramMedia>>> getUserRecentMedia(
            @Path("userId") String userId,
            @Query("access_token") String accessToken);

    @GET("users/self/media/liked")
    Single<InstagramResponse<List<InstagramMedia>>> getMyLikedMedia(
            @Query("access_token") String accessToken);

    @GET("users/self/follows")
    Single<InstagramResponse<List<InstagramUserDetails>>> getFollowedUsers(
            @Query("access_token") String accessToken);

    @FormUrlEncoded
    @POST("media/{mediaId}/likes")
    Single<InstagramResponse<Void>> likeMedia(
            @Path("mediaId") String mediaId,
            @Field("access_token") String accessToken);

    @DELETE("media/{mediaId}/likes")
    Single<InstagramResponse<Void>> unlikeMedia(
            @Path("mediaId") String mediaId,
            @Query("access_token") String accessToken);
}

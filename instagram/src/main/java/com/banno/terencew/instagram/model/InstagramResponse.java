package com.banno.terencew.instagram.model;

import com.google.gson.annotations.SerializedName;

/**
 * Every response from Instagram's REST API is wrapped in this
 */
public class InstagramResponse<T> {
    @SerializedName("data") private T data;

    public T getData() {
        return data;
    }
}

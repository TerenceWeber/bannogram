package com.banno.terencew.instagram.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * A single Instagram media post
 */
public class InstagramMedia {
    @SerializedName("id") private String id;
    @SerializedName("images") private InstagramImages images;
    @SerializedName("type") private String type;
    @SerializedName("caption") InstagramCaption caption;
    @SerializedName("user") InstagramUserDetails user;
    @SerializedName("location") InstagramLocation location;
    @SerializedName("tags") List<String> tags;
    @SerializedName("user_has_liked") boolean hasUserLiked;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public InstagramImages getImages() {
        return images;
    }

    public void setImages(InstagramImages images) {
        this.images = images;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public InstagramCaption getCaption() {
        return caption;
    }

    public void setCaption(InstagramCaption caption) {
        this.caption = caption;
    }

    public InstagramUserDetails getUser() {
        return user;
    }

    public void setUser(InstagramUserDetails user) {
        this.user = user;
    }

    public InstagramLocation getLocation() {
        return location;
    }

    public void setLocation(InstagramLocation location) {
        this.location = location;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public boolean hasUserLiked() {
        return hasUserLiked;
    }

    public void setHasUserLiked(boolean hasUserLiked) {
        this.hasUserLiked = hasUserLiked;
    }
}

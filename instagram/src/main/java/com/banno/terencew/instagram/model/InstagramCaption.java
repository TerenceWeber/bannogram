package com.banno.terencew.instagram.model;

import com.google.gson.annotations.SerializedName;

/**
 * A container for a caption
 */
public class InstagramCaption {
    @SerializedName("text") private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}

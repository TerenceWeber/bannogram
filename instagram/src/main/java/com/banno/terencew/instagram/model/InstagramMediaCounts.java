package com.banno.terencew.instagram.model;

import com.google.gson.annotations.SerializedName;

/**
 * The numbers for various statistics for an Instagram user
 */
public class InstagramMediaCounts {
    @SerializedName("media") private int media;
    @SerializedName("follows") private int follows;
    @SerializedName("followed_by") private int followedBy;

    public int getMedia() {
        return media;
    }

    public void setMedia(int media) {
        this.media = media;
    }

    public int getFollows() {
        return follows;
    }

    public void setFollows(int follows) {
        this.follows = follows;
    }

    public int getFollowedBy() {
        return followedBy;
    }

    public void setFollowedBy(int followedBy) {
        this.followedBy = followedBy;
    }
}

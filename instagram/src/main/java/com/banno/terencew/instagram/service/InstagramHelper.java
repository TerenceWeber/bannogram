package com.banno.terencew.instagram.service;

import android.util.Log;

import com.banno.terencew.instagram.model.InstagramMedia;
import com.banno.terencew.instagram.model.InstagramResponse;
import com.banno.terencew.instagram.model.InstagramUserDetails;

import java.util.List;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * A wrapper around the Instagram service
 * TODO: handle access token expired error. Check the HTTP error code, parse the response, and publish an event to MainActivity if expired
 */
public class InstagramHelper {
    private final static String BASE_URL = "https://api.instagram.com/v1/";

    private final InstagramService service;
    private final String accessToken;

    /**
     * Logcat logger for OkHttp
     * Necessary when in an Android library module
     */
    public class LogcatLogger implements HttpLoggingInterceptor.Logger {
        @Override
        public void log(String message) {
            Log.i(InstagramHelper.class.getName(), message);
        }
    }

    /**
     * All Observables run on the io thread and publish on the Android main thread
     * @param accessToken Instagram access token. Since every call uses access token, initialize it once here
     */
    public InstagramHelper(String accessToken) {
        this.accessToken = accessToken;

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor(new LogcatLogger());
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build();

        this.service = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(InstagramService.class);
    }

    public Single<InstagramUserDetails> getMyUserDetails() {
        return service.getMyUserDetails(accessToken)
                .map(response -> response.getData())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Single<List<InstagramMedia>> getMyRecentMedia() {
        return service.getMyRecentMedia(accessToken)
                .map(response -> response.getData())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Single<List<InstagramMedia>> getMyLikedMedia() {
        return service.getMyLikedMedia(accessToken)
                .map(response -> response.getData())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Single<InstagramResponse<Void>> likeMedia(String mediaId, boolean like) {
        return (like
            ? service.likeMedia(mediaId, accessToken)
            : service.unlikeMedia(mediaId, accessToken))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Single<List<InstagramUserDetails>> getFollowedUsers() {
        return service.getFollowedUsers(accessToken)
                .map(response -> response.getData())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Single<List<InstagramMedia>> getUserRecentMedia(String userId) {
        return service.getUserRecentMedia(userId, accessToken)
                .map(response -> response.getData())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}

package com.banno.terencew.instagram.model;

import com.google.gson.annotations.SerializedName;

/**
 * A set of Instagram images of different sizes
 */
public class InstagramImages {
    @SerializedName("low_resolution") private InstagramImage lowResolution;
    @SerializedName("thumbnail") private InstagramImage thumbnail;
    @SerializedName("standard_resolution") private InstagramImage standardResolution;

    public InstagramImage getLowResolution() {
        return lowResolution;
    }

    public void setLowResolution(InstagramImage lowResolution) {
        this.lowResolution = lowResolution;
    }

    public InstagramImage getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(InstagramImage thumbnail) {
        this.thumbnail = thumbnail;
    }

    public InstagramImage getStandardResolution() {
        return standardResolution;
    }

    public void setStandardResolution(InstagramImage standardResolution) {
        this.standardResolution = standardResolution;
    }
}

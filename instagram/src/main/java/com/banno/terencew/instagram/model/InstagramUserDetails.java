package com.banno.terencew.instagram.model;

import com.google.gson.annotations.SerializedName;

/**
 * Details for an Instagram user
 */
public class InstagramUserDetails {
    @SerializedName("id") private String id;
    @SerializedName("username") private String username;
    @SerializedName("full_name") private String fullName;
    @SerializedName("profile_picture") private String profilePicUrl;
    @SerializedName("bio") private String bio;
    @SerializedName("website") private String website;
    @SerializedName("counts") private InstagramMediaCounts mediaCounts;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getProfilePicUrl() {
        return profilePicUrl;
    }

    public void setProfilePicUrl(String profilePicUrl) {
        this.profilePicUrl = profilePicUrl;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public InstagramMediaCounts getMediaCounts() {
        return mediaCounts;
    }

    public void setMediaCounts(InstagramMediaCounts mediaCounts) {
        this.mediaCounts = mediaCounts;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object compareObj) {
        if (compareObj == null || !(compareObj instanceof InstagramUserDetails))
            return false;

        return ((InstagramUserDetails) compareObj).id.equals(this.id);
    }
}
